def suma(a: int, b: int) -> int:
    return a+b


def resta(a: int, b: int) -> int:
    return a-b


print(suma(1, 2))
print(suma(2, 3))
print(resta(3, 2))
print(resta(5, 6))
